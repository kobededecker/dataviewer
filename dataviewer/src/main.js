import Vue from 'vue'
import App from './App.vue'
import _ from "lodash";
import {csv} from "d3-request"
import api from "./api-axios"

Vue.prototype.$_ = _
Vue.prototype.$csv = csv
Vue.prototype.$api = api

export default new Vue({
  el: '#app',
  render: h => h(App)
})
