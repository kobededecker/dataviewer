import axios from 'axios';
import app from './main'; // import the instance

const instance = axios.create({
    baseURL: process.env.VUE_APP_API_URL
});

export default instance; // export axios instace to be imported in your app