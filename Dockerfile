FROM python:3
RUN pip install Dataviewer
# Mount data folder under /data
WORKDIR /data
CMD ["python", "/usr/local/bin/viewer.py", "/data", "-H", "0.0.0.0"]